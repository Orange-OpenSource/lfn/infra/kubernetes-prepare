# :warning: Not maintained anymore  :warning:

This is not maintained anymore, please use
https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes-prepare-role/
which is the same (with new features), but in a role.

# Kubernetes Preparation

## Introduction

Aim is to propose a role that prepare servers before installing Kubernetes.

you'll need a playbook and an inventory to launch it. You also need a
description of your platform, following "OPNFV standard" (PDF file).

the inventory needs a `k8s-cluster` group where the Kubernetes controllers and
node servers belongs to.

then you can launch it: `ansible-playbook -i inventory playbook.yml`.

## Inventory example

```Ansible
[kube-master]
control01
[monitoring]
control01
[kube-node]
compute01
compute02
compute03
compute04
compute05
compute06
compute07
compute08
compute09
compute10
compute11
compute12
[etcd]
control01

[k8s-cluster:children]
kube-master
kube-node
monitoring
```

## Playbook example

```YAML
---
- hosts: k8s-cluster
  gather_facts: "no"
  vars_files:
    - "vars/pdf.yml"
  roles:
    - role: kubernetes-prepare
```

## TODO

- [ ] Installation on Centos
